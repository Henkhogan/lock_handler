import logging
logger = logging.getLogger(__name__)

from threading import Lock, Condition

class LockHandler:
	logger = logging.getLogger(f'{__name__}.LockHandler')
	
	_blocking_default = False
	_wait_timeout_default = 5
	
	def __init__(self, wait_timeout = None):
		self._lock = Lock()
		self._condition = Condition(self._lock)
		self.wait_timeout = wait_timeout if wait_timeout != None else self._wait_timeout_default

	def acquire_lock(self):
		try:
			return self._condition.acquire(blocking = self._blocking_default)
		except TypeError:
			if not callable(self._condition):
				self.__init__(self.wait_timeout)
				self.logger.warn('reinitialized LockHandler to recover assumed unpickle error')
				return self._condition.acquire(blocking = self._blocking_default)
		except Exception:
			raise		
				
	def release_lock(self):
		self._condition.release()
					
	def is_locked(self):
		return self._lock.locked()
		
	def wait_unlock(self, timeout = None):
		if timeout == None:
			timeout = self._wait_timeout_default
		self._condition.wait_for(predicate = self._lock.locked, timeout = timeout)
		
	def wait_if_locked(self, timeout = None):
		if timeout == None:
			timeout = self._wait_timeout_default
		if self.is_locked():
			self.wait_unlock(timeout)
			return True
		return False

	def __enter__(self):
		self.acquire_lock()

	def __exit__(self, exc_type, exc, exc_tb):
		self.release_lock()
		
class LockHandlerBlocking(LockHandler):
	_blocking_default = True
