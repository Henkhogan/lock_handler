import logging
import unittest


logger = logging.getLogger('lock_handler')
level = logging.DEBUG
logger.setLevel(level)
logger.root.setLevel(level)

if __name__ == '__main__':
	unittest.main('tests.tests')
