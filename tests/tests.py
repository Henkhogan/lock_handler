from unittest import TestCase
import asyncio

from lock_handler import (
	LockHandler,
	LockHandlerBlocking
	)

class LockHandlerTestCase(TestCase):
	def get_instance(self):
		return LockHandler()

	def test_init(self):
		c = self.get_instance()
		self.assertFalse(c.is_locked())

	def test_acquire_and_release_lock(self): 
		c = self.get_instance()
		self.assertTrue(c.acquire_lock())
		self.assertFalse(c.acquire_lock())
		c.release_lock()
		self.assertTrue(c.acquire_lock())		

	def test_is_locked(self):
		c = self.get_instance()
		c.acquire_lock()
		self.assertTrue(c.is_locked())
		c.release_lock()
		self.assertFalse(c.is_locked())
				
	def test_wait_unlock(self):
		event_loop = asyncio.new_event_loop()
		asyncio.set_event_loop(event_loop)
		c = self.get_instance()
		c.acquire_lock()
		counter = [0]
		
		async def release_lock(c,counter):
			counter[0] = counter[0] + 1
			await asyncio.sleep(1)
			counter[0] = counter[0] + 1
			c.release_lock()
		async def wait_lock(c):
			c.wait_unlock()
			
		loop = asyncio.get_event_loop()
		loop.run_until_complete(asyncio.gather(
			wait_lock(c),
			release_lock(c, counter)
			))
		loop.close()
		
		self.assertEqual(2, counter[0])
	
	def test_wait_if_locked_unlocked(self):
		c = self.get_instance()
		self.assertFalse(c.wait_if_locked())
		
	def test_wait_if_locked_locked(self):
		event_loop = asyncio.new_event_loop()
		asyncio.set_event_loop(event_loop)
		l = self.get_instance()
		l.acquire_lock()
		wait = [None]
		counter = [0]
		
		async def release_lock(l,counter):
			counter[0] = counter[0] + 1
			await asyncio.sleep(1)
			counter[0] = counter[0] + 1
			l.release_lock()
			
		async def wait_lock(l, wait):
			wait[0] = l.wait_if_locked(100)
			
		loop = asyncio.get_event_loop()
		loop.run_until_complete(asyncio.gather(
			wait_lock(l, wait),
			release_lock(l, counter)
			))
		loop.close()
		self.assertEqual(True,wait[0])

	def test_context_manager(self):
		c = self.get_instance()
		self.assertFalse(c.is_locked())
		with c:
			self.assertTrue(c.is_locked())
		self.assertFalse(c.is_locked())
			